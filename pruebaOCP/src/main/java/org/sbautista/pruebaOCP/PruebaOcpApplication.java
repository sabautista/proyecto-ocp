package org.sbautista.pruebaOCP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaOcpApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaOcpApplication.class, args);
	}

}

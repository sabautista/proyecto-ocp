package org.sbautista.pruebaOCP;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import lombok.Data;

@Configuration
@ConfigurationProperties(prefix = "bean")
@Data
public class MyConfig {
  
  private String message = "a message that can be changed live";
  
}
